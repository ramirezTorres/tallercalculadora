package com.example.usuario.calculadora;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button btnSuma, btnResta, btnMultiplica, btnDivide;
    private EditText campoNumero1, campoNumero2;
    private TextView resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        btnSuma = (Button) findViewById(R.id.botonsuma);
        btnResta = (Button) findViewById(R.id.botonresta);
        btnMultiplica = (Button) findViewById(R.id.botonmultiplica);
        btnDivide = (Button) findViewById(R.id.botondivide);

        campoNumero1 = (EditText) findViewById(R.id.numero1);
        campoNumero2 = (EditText) findViewById(R.id.numero2);

        resultado = (TextView) findViewById(R.id.resultado);

        btnSuma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int num1, num2, res;

                num1 = Integer.parseInt(campoNumero1.getText().toString());
                num2 = Integer.parseInt(campoNumero2.getText().toString());

                res = num1 + num2;

                resultado.setText(String.valueOf(res));
            }
        });

    }
}
